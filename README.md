# Discord ETH Wallet Verification Bot

This is a Discord bot built using the Discord.js library and the Alchemy SDK that verifies Ethereum wallet addresses for server members. 

## Prerequisites 
- Node.js
- Discord API key
- Alchemy API key

## Libraries used
- Discord.js
- Alchemy-sdk

## Installation 
1. Install the required packages:
```
npm install
```
2. Replace `"MTA3MDMxMDg2MTgwMzA5NDA0Nw.G2sahp.JXCmAcEYlHGEz-uGePeuJ3zjUebm1EzEn66eSw"` with your Discord API key and `"mgVo5d5t7BrzlHeOHNEiX9_lD4dD4Tbv"` with your Alchemy API key in the `index.js` file.

## Usage
1. Start the bot:
```
node index.js
```
2. The bot will welcome new members to the server and ask them to provide their Ethereum wallet address. 
3. If the member provides a valid Ethereum wallet address with a non-zero balance or NFT, the bot will leave them in the server. 
4. If the member provides an invalid Ethereum wallet address or an address with a zero balance and no NFTs, the bot will kick them from the server.

## To create a Discord bot and add it to your server, follow these steps:

-Create a Discord account if you don't have one and sign in.

-Go to the Discord Developer Portal (https://discord.com/developers/applications) and create a new application.

-Click on the "Bot" section and then click on the "Add Bot" button to create a new bot.

-Invite the bot to your server by generating an OAuth2 URL. You can select the permissions you want your bot to have and then click on the "Generate OAuth2 URL" button.

-Copy the URL and paste it into your browser to invite the bot to your server.

-Once the bot is added to your server, you can start coding and adding features to it.

-Use the Discord API and a programming language such as JavaScript to control the bot.

-Deploy the bot to a hosting platform such as Heroku to run it 24/7.

-Test your bot on your server to make sure it's working as intended.

-That's it! Now you can have your own custom Discord bot up and running on your server.





